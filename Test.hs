f :: Int -> Int
f x = x+1
{-# NOINLINE f #-}

main :: IO ()
main = print (f 42)
